## Windows Privesc Capstone Notes

This repo is a collection of my notes/writeups for the **Windows Privilege Escalation** Course, specifically for the _Capstone Challenge_ Section.  
I have created this repo so I can get better at note-taking.  
The course runs through numerous methods of escalating privilege on vulernable Windows VMs. You can check it out [here](https://www.udemy.com/share/102YD8B0sYeVlXRQ==/).  
The **Capstone Challenge** section is 5 vulnerable Windows boxes that students are tasked with mastering:

* [Arctic](https://gitlab.com/cnose/wpe-capstone/-/tree/master/1-ARCTIC) (HTB)
* [Bastard](https://gitlab.com/cnose/wpe-capstone/-/tree/master/2-BASTARD) (HTB)
* [Alfred](https://gitlab.com/cnose/wpe-capstone/-/tree/master/3-ALFRED) (THM)
* [Bastion](https://gitlab.com/cnose/wpe-capstone/-/tree/master/4-BASTION) (HTB)
* [Querier](https://gitlab.com/cnose/wpe-capstone/-/tree/master/5-QUERIER) (HTB)

The boxes are roughly in order of difficulty. Check this readme as each box is completed and click on the ref links as each writeup is added.
