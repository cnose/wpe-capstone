# BASTION | 10.10.10.134

------------------------------

## Initial Enumeration - nmap scan

### Open Ports
* 22
* 135
* 139
* 445
* 5985
* 47001
* 49666-49670

### Service and OS Discovery
**OS:** Windows Server Standard 2016 14393
#### Services
22: OpenSSH for Windows 7.9  
135: MSRPC  
139 & 445: Samba 2 - **message signing not required**  
5985: SSDP (perhaps actually WinRM)  
47001: SSDP  
49664-49670: MSRPC

## Service Enumeration
### Samba (ports 139/445)

![samba share](smb/smb-dir.png)

Using smbclient, the server allows remote viewing. There are several files and dirs, notably a **Backups** folder, which contains several **VHD** files.
Also, there is a note.txt file which contains
> Sysadmins: please don't transfer the entire backup file locally, the VPN to the subsidiary office is too slow.

Researching the web reveals that VHD files are virtual hard disks, which could mean the files are entire disk images. They can be mounted like NFS but require some tools to be installed on the Kali machine.
> apt install libguestfs-tools
>
> apt install cifs-tools

(This is where I ran into the issue of the fact that the libguestfs package was removed from the Kali repo literally a few days ago, so I am now unable to mount the virtual disk images in Linux. Gonna skip ahead to the part where the user's creds have been cracked....)

The process for attaining the User's (L4mpje) credentials is as follows:

* Mount the VHD files 
* Explore the filesystem - nothing is forbidden by permissions issues in this case
* Find the SAM and SYSTEM hashes in the **C:\Windows\System32\config** folder
* Crack the hashes using hashcat (hashcat -m 1000 hash /usr/share/wordlists/rockyou.txt -O)

The user's password is **bureaulampje**

OK, now back to actual work.

## Initial Access

> ssh l4mpj3@10.10.10.134 gains us access to a shell

![shell access](shell.png)

## Enumeration

I notice pretty quickly that there are a few commands, typically used for enumeration, that I cannot run, which will make this more of a challenge.

![basic enum](lampje-enum.png)

However, I can still read the user flag, so that's good and done.

![flag](user-flag.png)

Searching manually for passwords finds something for Remote Desktop/VNC - yhgmiu5bbua
mU3qMUKc/uYDdmbMrJZ/JvR1kYe4Bhiu8bXybLxVnO0U9fKRylI7NcB9QuRsZVvla8esB - for the user

and aEWNFV5uGcjUHF0uS17QTdT9kVqtKCPeoC0Nw5dmaPFjNQ2kt/zO5xDqE4HdVmHAowVRdC7emf7lWWA10dQKiw== for the Administrator

These hashes are connected to a remote desktop program called mRemoteNG - might be worth looking into this more.

I managed to get winPEAS onto the machine using a powershell command (user wa sunable to run certutil) - there was not much to look at as this user seems pretty locked down. I did see this, however:

![winpeas](winpeas1.png)

Which leads me to the .bat file containing this command:

![bat](script-bat.png)

Which is related to the backups smb share folder. So, what we have here is a batch script running on startup that I can write to and control.  
I'm not sure if this is the privesc path, because usually online boxes like this do not allow you to restart. I must be missing something here...

After getting PowerUp on the system and trying to run that, the only thing that came up was a potential .dll hijacking vuln:

![hijack](powerup-hijack.png)

This got me nowhere as I did not know how to find the exe that calls this .dll file, so I went back to looking at the hashes I found earlier.

## mRemoteNG

This is a program that is kind of a wrapper for many remote programs like VNC, RDP, SSH etc etc. Turns out it has a vulnerability in which you can extract the hashes (like I did), then they can be easily decrypted with a known key.  
After searching online for vulnerabilties/exploits I found a small [python program](https://github.com/haseebT/mRemoteNG-Decrypt) that you feed the hash to and it decrypts it for you.  
Throwing it the user hash, I got
> bureaulampje

back, which means it's working. So, now on to the admin hash:

![admin](admin-pw.png)

Got it! Now to put it to use. Connecting via ssh grants me access!

![iamroot](admin-shell.png)

Have to admit I was a little suspicious, only being the Administrator, because normally it is NT AUTHORITY\SYSTEM, so I went for the flag right away to confirm.

![flag](root-flag.png)

And that's that.

## Conclusion

This was the most challenging and most satisfying box yet, even though I had to skip the part with the mounting of the VHD files, which sucks, but what can you do? It was interesting because several of the tried and true commands and automated tools did not work or sent me down rabbit holes, and what saved the day was old fashioned enumeration.  
I literally searched for the string _password_ in the entire system, and waded through huge amounts of useless text until somehow the hashes stood out among the text.  
I really should have checked what Programs were installed first, which would have saved me a lot of time and effort. But now I have that in the front of my mind for the next machines.
